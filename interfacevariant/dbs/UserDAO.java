package com.deveducation.www.interfacevariant.dbs;

import com.deveducation.www.interfacevariant.data.CommonData;
import com.deveducation.www.interfacevariant.impl.Controller;

public class UserDAO extends Controller {
    @Override
    public void delete() {
        System.out.println("method delete from UserDaO class was called");
    }

    @Override
    public void update() {
        System.out.println("method update from UserDaO class was called");
    }

    @Override
    public void clearAll() {
        System.out.println("method clearAll from UserDaO class was called");
    }

    @Override
    public void selectAll() {
        System.out.println("method selectAll from UserDaO class was called");
    }

    @Override
    public void connect() {
        System.out.println("method connect from UserDaO class was called");
    }

    @Override
    public void disconnect() {
        System.out.println("method disconnect from UserDaO class was called");
    }

    @Override
    public void insert() {
        System.out.println("method insert from UserDaO class was called");
    }
}
