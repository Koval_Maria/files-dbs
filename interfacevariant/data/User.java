package com.deveducation.www.interfacevariant.data;

public class User extends CommonData {
    private String login;
    private String password;
    private int age;

    public User(int id, String login, String password, int age) {
        super(id);
        this.login = login;
        this.password = password;
        this.age = age;
    }
}
