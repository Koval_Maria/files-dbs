package com.deveducation.www.interfacevariant.impl;

import com.deveducation.www.interfacevariant.data.Note;
import com.deveducation.www.interfacevariant.files.NoteDAO;
import java.util.Scanner;

public class Controller {
    private IFile executor;
    private NoteDAO noteDAO;
    private boolean isOver = false;


    public Controller() {
        noteDAO = new NoteDAO();
    }

    void switchFileName(String fileName) {
        switch (fileName) {
            case "note.txt":
                executor = new NoteDAO();
                break;
            case "inventory.txt":
                executor = new NoteDAO();
                break;
        }
    }

    void start() {
        Scanner scanner = new Scanner(System.in);
        while (!isOver) {
            switchFileName("inventory.txt");
            String message1 = scanner.next();
            String message2 = scanner.next();

        }
    }

    public void delete() {
        System.out.println("Some message");
    }

    public void update() {
        System.out.println("Some message");
    }

    public void clearAll() {
        System.out.println("Some message");
    }

    public void selectAll() {
        System.out.println("Some message");
    }

    public void connect() {
        System.out.println("Some message");
    }


    public void disconnect() {
        System.out.println("Some message");
    }

    public void insert() {
        System.out.println("Some message");
    }

}




