package com.deveducation.www.interfacevariant.impl;

import com.deveducation.www.interfacevariant.data.CommonData;

public interface ICrud {
    void delete(int id);

    void update(CommonData data);

    void clearAll();
}
